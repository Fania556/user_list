import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {List} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import {grey400, darkBlack, lightBlack} from 'material-ui/styles/colors';
import img from '../../images/1.jpg' ;
import List_item from './list_item'

export default class User_List extends Component {
  render() {
    const {users,actions} = this.props;
    const tmp = users.map(function (el,i) {
      return (
        <div key={i}>
          <List_item
            item={el}
            img={img}
            actions={actions}
            index={i}
            />
        </div>
      )
    })
    return (
      <div className="list_container">
          <List>
            <Subheader>Today</Subheader>
            {tmp}
          </List>
      </div>
    );
  }
}
