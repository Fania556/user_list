import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class More extends Component {
  render(){
  const {name,surname,about} = this.props.current;
    return(
      <div className="more">
        <h1>{name+" "+surname}</h1>
        <p>{about}</p>
      </div>
    )
  }
}
