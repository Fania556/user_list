import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';
import {grey400, darkBlack, lightBlack} from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();




export default class List_item extends Component {

  render() {
    const {img,item,actions,index} = this.props;
    function Pick(e){
      actions.pick_item({index});
    }
    function Edit() {
      let newName = prompt("Введите новое имя пользователя");
      console.log(newName)
      actions.rename_item({name :newName,index:{index}});
    }
    function Delete() {
      if(confirm("Вы уверены что хотите удалить элемент?")) actions.delete_item({index});
    }
    const iconButtonElement = (
      <IconButton
        touch={true}
        tooltip="more"
        tooltipPosition="bottom-left"
      >
        <MoreVertIcon color={grey400} />
      </IconButton>
    );
    const rightIconMenu = (
      <IconMenu iconButtonElement={iconButtonElement}
      anchorOrigin={{horizontal: 'left', vertical: 'top'}}
      targetOrigin={{horizontal: 'left', vertical: 'top'}}
      >
        <MenuItem onClick={Edit}>Edit</MenuItem>
        <MenuItem onClick={Delete}>Delete</MenuItem>
      </IconMenu>
    );
    return (
      <div >
      <ListItem
      leftAvatar={<Avatar src={img}/>}
      rightIconButton={rightIconMenu}
      primaryText={item.name+" "+item.surname}
      secondaryText={
        <p onClick={Pick}>

        <span style={{color: darkBlack}}>{item.pt}</span><br />
        {item.about}
        </p>
      }
      secondaryTextLines={2}
      />
      <Divider inset={true} />
      </div>
    )
  }
}
