import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {createStore} from 'redux';
import {connect} from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import User_List from '../components/list';
import More from '../components/more';
import {bindActionCreators} from 'redux';
import {pick_item, delete_item, rename_item} from '../actions'
class App extends Component {
    render() {
        const {users, current,actions} = this.props;
        return (
            <MuiThemeProvider>
                <div className="ul_container">
                    <User_List users={users} actions={actions} />
                    <More current={current}></More>
                </div>
            </MuiThemeProvider>
        );
    }
}

function mapStateToProps(state) {
    return {users: state.users, current: state.current}
}
function mapDispatchToProps(dispatch) {
    return {
        actions: {
            pick_item: bindActionCreators(pick_item, dispatch),
            delete_item: bindActionCreators(delete_item, dispatch),
            rename_item: bindActionCreators(rename_item, dispatch)
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(App)
