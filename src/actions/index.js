export const DELETE_ITEM = 'DELETE_ITEM';
export const RENAME_ITEM = 'RENAME_ITEM';
export const PICK_ITEM = 'PICK_ITEM';

export function delete_item(index) {
  return {type: DELETE_ITEM, data : index};
}
export function rename_item(data) {
  return {type: RENAME_ITEM, data : data};
}
export function pick_item(index) {
  return {type: PICK_ITEM, data : index};
}
