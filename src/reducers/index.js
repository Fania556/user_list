import {
    DELETE_ITEM,
    RENAME_ITEM,
    PICK_ITEM
} from '../actions';
const initialState = {
    users: [{
            'name': 'Alex ',
            'surname': 'Johnson',
            'photo': './images/1.jpg',
            'about': 'Web-designer, JS-Developer and just a good man',
            'pt': 'BBC News'
        },
        {
            'name': 'Selena',
            'surname': 'Johnson',
            'photo': './images/1.jpg',
            'about': 'CEO, CMO',
            'pt': 'BBC News'
        },
        {
            'name': 'Paul ',
            'surname': 'Thompson',
            'photo': './images/1.jpg',
            'about': 'Team-leader, Manager',
            'pt': 'BBC News'
        },
        {
            'name': 'Nick',
            'surname': 'Myers',
            'photo': './images/1.jpg',
            'about': 'Artist, Web-designer',
            'pt': 'BBC News'
        },
        {
            'name': 'George',
            'surname': 'Phillip',
            'photo': './images/1.jpg',
            'about': 'C#-developer, .NET-Developer and ADO.NET - developer',
            'pt': 'BBC News'
        }
    ],
    current : {
      'name': '',
      'surname': '',
      'photo': '',
      'about': '',
      'pt': ''
    }
};

export default function userList(state = initialState, action) {
    switch (action.type) {
        case DELETE_ITEM:
            return { ...state,
                ...{
                    users: state.users.filter(function(el, index) {
                        return index != action.data.index;
                    }),
                    current : initialState.current
                }
            };
            break;
        case RENAME_ITEM:
            console.log(state);
            console.log(action.data.index.index)
            return { ...state,
                ...{
                    users: state.users.map(function(el, index) {
                      if(index == action.data.index.index) { el.name = action.data.name ;return el }
                      else return el;
                    })
                }
            }
            break;
        case PICK_ITEM:
            return { ...state,
                ...{
                    current: state.users[action.data.index]
                }
            }
            break;
        default:
            return state;
    }
}
